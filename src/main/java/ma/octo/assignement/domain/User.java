package ma.octo.assignement.domain;

import lombok.*;
import ma.octo.assignement.config.WebSecurityConfig;
import ma.octo.assignement.dto.UserDto;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "USER")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class User implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 10, nullable = false, unique = true)
  private String username;

  @Column(length = 10, nullable = false)
  private String gender;

  @Column(length = 60, nullable = false)
  private String lastname;

  @Column(length = 60, nullable = false)
  private String firstname;

  @Column(nullable = false)
  private String password;

  @Temporal(TemporalType.DATE)
  private Date birthdate;


  public void setPassword(String password) {
    this.password = WebSecurityConfig.bCryptPasswordEncoder().encode(password);
  }

  public static UserDto map(User user) {
    UserDto userDto = new UserDto();

    userDto.setUsername(user.getUsername());
    userDto.setFirstname(user.getFirstname());
    userDto.setLastname(user.getLastname());
    userDto.setGender(user.getGender());
    userDto.setPassword(user.getPassword());
    userDto.setBirthdate(user.getBirthdate());

    return userDto;
  }
}
