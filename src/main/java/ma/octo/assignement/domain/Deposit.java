package ma.octo.assignement.domain;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.octo.assignement.dto.DepositDto;

@Entity
@Table(name = "DEP")
@NoArgsConstructor
@Getter
@Setter
public class Deposit extends Transaction {

  @Column
  private String nomPrenomEmetteur;

  @Builder
  public Deposit(Long id, BigDecimal montantTransfer, Date dateExecution, Compte compteBeneficiaire, String motifTransfer,
		String nom_prenom_emetteur) {
	super(id, montantTransfer, dateExecution, compteBeneficiaire, motifTransfer);
	this.nomPrenomEmetteur = nom_prenom_emetteur;
  }
    public static DepositDto map(Deposit deposit) {
        DepositDto depositDto = new DepositDto();
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifTransfer());
        depositDto.setMontant(deposit.getMontantTransfer());
        depositDto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        depositDto.setRib(deposit.getCompteBeneficiaire().getRib());
        return depositDto;
    }
  
}