package ma.octo.assignement.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.octo.assignement.dto.TransferDto;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "TRAN")
@NoArgsConstructor
@Getter
@Setter
public class Transfer extends Transaction{
  @ManyToOne
  private Compte compteEmetteur;

  @Builder
  public Transfer(Long id, BigDecimal montantTransfer, Date dateExecution, Compte compteBeneficiaire,
			String motifTransfer, Compte compteEmetteur) {
		super(id, montantTransfer, dateExecution, compteBeneficiaire, motifTransfer);
		this.compteEmetteur = compteEmetteur;
	}

    public static TransferDto map(Transfer transfer) {
        TransferDto transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMontant(transfer.getMontantTransfer());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMotif(transfer.getMotifTransfer());

        return transferDto;

    }
}