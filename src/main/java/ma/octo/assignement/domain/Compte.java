package ma.octo.assignement.domain;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.octo.assignement.dto.CompteDto;
import java.math.BigDecimal;

@Entity
@Table(name = "COMPTE")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Compte {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;
  
  
  @Column
  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal solde;

  @ManyToOne()
  @JoinColumn(name = "user_id")
  private User user;

  public static CompteDto map(Compte compte) {
    CompteDto compteDto = new CompteDto();
    compteDto.setNrCompte(compte.getNrCompte());
    compteDto.setRib(compte.getRib());
    compteDto.setSolde(compte.getSolde());
    compteDto.setUserDto(User.map(compte.getUser()));
    return compteDto;
  }
}
