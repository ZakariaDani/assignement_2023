package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRepository extends JpaRepository<Compte, Long> {
	Optional<Compte> findByNrCompte(String nrCompte);
	Optional<Compte> findByRib(String rib);
}
