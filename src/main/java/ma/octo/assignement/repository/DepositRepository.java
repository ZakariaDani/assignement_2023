package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

public interface DepositRepository  extends JpaRepository<Deposit, Long> {
	
	@SuppressWarnings("unchecked")
	Deposit save(Deposit deposit);

	List<Deposit> findAllByDateExecutionGreaterThanEqualAndDateExecutionLessThanAndCompteBeneficiaire_Rib(Date dateStart, Date dateEnd, String rib);
}