package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TransferRepository transferRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		User user1 = new User();
		user1.setUsername("user1");
		user1.setLastname("last1");
		user1.setPassword("1234");
		user1.setFirstname("first1");
		user1.setGender("Male");

		userRepository.save(user1);


		User user2 = new User();
		user2.setUsername("user2");
		user2.setLastname("last2");
		user2.setPassword("12345");
		user2.setFirstname("first2");
		user2.setGender("Female");

		userRepository.save(user2);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUser(user1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUser(user2);

		compteRepository.save(compte2);

		Transfer v = new Transfer();
		v.setMontantTransfer(BigDecimal.TEN);
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotifTransfer("Assignment 2021");

		transferRepository.save(v);
	}
}
