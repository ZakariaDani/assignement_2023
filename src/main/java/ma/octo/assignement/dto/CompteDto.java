package ma.octo.assignement.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class CompteDto {
	
	private String nrCompte;
	private String rib;
	private BigDecimal solde;
	private UserDto userDto;
	
}
