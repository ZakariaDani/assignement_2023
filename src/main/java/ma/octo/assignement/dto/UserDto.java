package ma.octo.assignement.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class UserDto {
	private String username;
	private String gender;
	private String lastname;
	private String firstname;
	private String password;
	private Date birthdate;
}
