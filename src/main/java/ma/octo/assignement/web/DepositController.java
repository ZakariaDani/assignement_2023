package ma.octo.assignement.web;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/deposits")
@Slf4j
public class DepositController {
	
    private DepositService depositService;
    private AuditService auditService;
    
    
    @GetMapping
    public ResponseEntity<List<DepositDto>> loadAllDeposit() {
        Optional<List<DepositDto>> optionalDepositDtos = depositService.loadAllDeposit();
        return ResponseEntity.status(HttpStatus.OK).body(optionalDepositDtos.get());
    }
    
    @PostMapping
    public ResponseEntity<Void> createTransaction(@RequestBody DepositDto depositDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        depositService.createDeposit(depositDto);
        auditService.audit("Deposit depuis " + depositDto.getNomPrenomEmetteur() + " vers " + depositDto
                .getRib() + " d'un montant de " + depositDto.getMontant()
                .toString(), EventType.DEPOSIT);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @GetMapping("/today/{rib}")
    public ResponseEntity<List<DepositDto>> loadAllTodayDeposit(@PathVariable String rib) {
        Optional<List<DepositDto>> optionalDepositDtos = depositService.loadAllTodayDepositWithRib(rib);
        return ResponseEntity.status(HttpStatus.OK).body(optionalDepositDtos.get());
    }
}