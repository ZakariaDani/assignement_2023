package ma.octo.assignement.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.service.CompteService;

@AllArgsConstructor
@RestController
@RequestMapping("/accounts")
@Slf4j
public class CompteController {
	
	private CompteService compteService;
		
	@GetMapping
    public ResponseEntity<List<CompteDto>> loadAllCompte() {
        Optional<List<CompteDto>> optionalCompteDtos = compteService.loadAllCompte();
        if(optionalCompteDtos.isPresent()) {
            log.info("Lister des comptes");
            return ResponseEntity.status(HttpStatus.OK).body(optionalCompteDtos.get());
        }else {
        	log.error("aucun comptes");
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }
    }

}