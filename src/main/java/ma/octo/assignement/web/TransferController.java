package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/transfers")
@Slf4j
public class TransferController {

    private final AuditService auditService;
    private final TransferService transferService;

    @GetMapping
    public ResponseEntity<List<TransferDto>> loadAllTransfer() {
        Optional<List<TransferDto>> optionalTransferDtos = transferService.loadAllTransfer();
        return ResponseEntity.status(HttpStatus.OK).body(optionalTransferDtos.get());
    }    

    @PostMapping
    public ResponseEntity<Void> createTransaction(@RequestBody TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{
    	transferService.createTransfer(transferDto);
        auditService.audit("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString(), EventType.TRANSFER);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }
}