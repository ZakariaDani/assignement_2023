package ma.octo.assignement.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.service.UserService;

@RestController
@RequestMapping("/users")
@Slf4j
@AllArgsConstructor
public class UserController {
	
	private UserService userService;
		
	
	 @GetMapping
	    public ResponseEntity<List<UserDto>> loadAllUsers() {
	        Optional<List<UserDto>> optionalUserDtos = userService.loadAllUsers();
	        if(optionalUserDtos.isPresent()) {
	            log.info("Lister des utilisateurs");
	            return ResponseEntity.status(HttpStatus.OK).body(optionalUserDtos.get());
	        }else {
	        	log.error("aucun utilisateur");
	            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
	        }
	    }

}
