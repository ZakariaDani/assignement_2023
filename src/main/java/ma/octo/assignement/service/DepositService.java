package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
@Transactional
public class DepositService {

    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private CompteRepository compteRepository;
    
    public static final int MONTANT_MAXIMAL = 10000;
    
    public Optional<List<DepositDto>> loadAllDeposit() {
        List<DepositDto> depositDtos = Optional.ofNullable(depositRepository.findAll()).orElse(Collections.emptyList()).stream()
                .map(Deposit::map)
                .collect(Collectors.toList());
        return Optional.of(depositDtos);
	}

    public Optional<List<DepositDto>> loadAllTodayDepositWithRib(String rib) {
        List<DepositDto> depositDtos = Optional.ofNullable(depositRepository.findAllByDateExecutionGreaterThanEqualAndDateExecutionLessThanAndCompteBeneficiaire_Rib(getStartAndEndDate().get(0), getStartAndEndDate().get(1), rib)).orElse(Collections.emptyList()).stream()
                .map(Deposit::map)
                .collect(Collectors.toList());
        return Optional.of(depositDtos);
    }
    

    public DepositDto createDeposit(DepositDto depositDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        if(depositDto.getRib() == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
        Optional<Compte> c1 = compteRepository.findByRib(depositDto.getRib());

        if (c1.isEmpty()) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }


        if (depositDto.getMontant() == null) {
        	log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (depositDto.getMontant().intValue() == 0) {
        	log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (depositDto.getMontant().intValue() < 10) {
        	log.error("Montant minimal de deposit non atteint");
            throw new TransactionException("Montant minimal de deposit non atteint");
        } else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
        	log.error("Montant maximal de deposit dépassé");
            throw new TransactionException("Montant maximal de deposit dépassé");
        }

        if (depositDto.getMotif().length() == 0) {
        	log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }
        if(this.loadAllTodayDepositWithRib(c1.get().getRib()).get().size() >= 3) {
            log.error("Nombre de deposit dépassé");
            throw new TransactionException("Nombre de deposit dépassé");
        }
        c1.get().setSolde(c1.get().getSolde().add(depositDto.getMontant()));
        compteRepository.save(c1.get());


        Deposit deposit = new Deposit();
        deposit.setDateExecution(depositDto.getDate());
        deposit.setCompteBeneficiaire(c1.get());
        deposit.setMotifTransfer(depositDto.getMotif());
        deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());
        deposit.setMontantTransfer(depositDto.getMontant());
        Deposit returnedDeposit = depositRepository.save(deposit);
        return Deposit.map(returnedDeposit);
    }

    public List<Date> getStartAndEndDate() {
        Date dateStart;
        Date dateEnd;
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        dateStart = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        dateEnd = calendar.getTime();
        List dateList = new ArrayList<>();
        dateList.add(dateStart);
        dateList.add(dateEnd);
        return dateList;
    }
}