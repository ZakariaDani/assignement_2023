package ma.octo.assignement.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ma.octo.assignement.domain.User;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.repository.UserRepository;

@Service
@AllArgsConstructor
public class UserService {
	
	 private UserRepository userRepository;
	 
	 public Optional<List<UserDto>> loadAllUsers() {
	        List<UserDto> userDtos = userRepository.findAll().stream()
	                .map(User::map)
	                .collect(Collectors.toList());
	        return Optional.of(userDtos);
		}


	public Optional<UserDto> getUserByUsername(String username) {
		return Optional.ofNullable(User.map(userRepository.findByUsername(username)));
	}
}
