package ma.octo.assignement.service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ma.octo.assignement.domain.Compte;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.repository.CompteRepository;

@Service
@AllArgsConstructor
public class CompteService {
	
	    private CompteRepository compteRepository;
	    
	    public Optional<List<CompteDto>> loadAllCompte() {
	        List<CompteDto> compteDtos = compteRepository.findAll().stream()
	                .map(Compte::map)
	                .collect(Collectors.toList());
	        return Optional.of(compteDtos);
		}

}
