package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;


import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;


@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class TransferService {
	
	private TransferRepository transferRepository;
	private CompteRepository compteRepository;
	public static final int MONTANT_MAXIMAL = 10000;

	public Optional<List<TransferDto>> loadAllTransfer() {
        List<TransferDto> transferDtos = Optional.ofNullable(transferRepository.findAll()).orElse(Collections.emptyList()).stream()
                .map(Transfer::map)
                .collect(Collectors.toList());
        return Optional.of(transferDtos);
	}
	
	public TransferDto createTransfer(TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		if(transferDto.getNrCompteEmetteur() == null || transferDto.getNrCompteBeneficiaire() == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
        Optional<Compte> compte1 = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Optional<Compte> c2 = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        if (compte1.isEmpty() || c2.isEmpty()) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }


        if (transferDto.getMontant() == null) {
        	log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() == 0) {
        	log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() < 10) {
        	log.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
        	log.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif().length() == 0) {
        	log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compte1.get().getSolde().intValue() < transferDto.getMontant().intValue()) {
        	log.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant");
        }

        compte1.get().setSolde(compte1.get().getSolde().subtract(transferDto.getMontant()));
        compteRepository.save(compte1.get());

        c2.get().setSolde(new BigDecimal(c2.get().getSolde().intValue() + transferDto.getMontant().intValue()));
        compteRepository.save(c2.get());

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(c2.get());
        transfer.setCompteEmetteur(compte1.get());
        transfer.setMontantTransfer(transferDto.getMontant());

        
        Transfer returnedTransfer = transferRepository.save(transfer);
        return Transfer.map(returnedTransfer);
	}
}