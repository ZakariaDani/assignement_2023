package ma.octo.assignement.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;

@ExtendWith(MockitoExtension.class)
public class TransferServiceTest {
	@InjectMocks
	private TransferService transferService;

	@Mock
	private TransferRepository transferRepository;

	@Mock
	private CompteRepository compteRepository;
	
	private Transfer transfer;
	private List<Optional<Compte>> listCompte;
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		Compte compte1 = Compte.builder()
				.rib("RIB1")
				.user(null)
				.solde(BigDecimal.valueOf(10000.00))
				.nrCompte("010000A000001000")
				.build();
		Compte compte2 = Compte.builder()
				.rib("RIB2")
				.user(null)
				.solde(BigDecimal.valueOf(40000.00))
				.nrCompte("010000B000001000")
				.build();
		listCompte = new ArrayList<>();
		listCompte.add(Optional.of(compte1));
		listCompte.add(Optional.of(compte2));
		transfer = Transfer.builder()
	    		.compteBeneficiaire(compte1)
	    		.compteEmetteur(compte2)
	    		.dateExecution(new Date())
	    		.motifTransfer("2024")
	    		.build();
	}
	
	@Test
    @DisplayName("Test save Transfer with amount greater than 10 and less than 10000")
    public void should_save_transfer_if_not_null_and_amount_greater_than_10_and_lesser_than_10000() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		this.transfer.setMontantTransfer(BigDecimal.valueOf(1000.00));
		when(compteRepository.findByNrCompte(any())).thenReturn(listCompte.get(0), listCompte.get(1));
        doReturn(transfer).when(transferRepository).save(any());

        TransferDto returnedTransfer = transferService.createTransfer(Transfer.map(transfer));
        verify(compteRepository, times(2)).findByNrCompte(any());
        Assertions.assertNotNull(returnedTransfer, "The saved transfer should not be null");
        Assertions.assertEquals(BigDecimal.valueOf(1000.00), returnedTransfer.getMontant(), "The amount");
    }
	
//	@Test
//    @DisplayName("Test not save Transfer with amount lesser than 10")
//    public void should_not_save_transfer_if_not_null_and_amount_lesser_than_10() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
//		this.transfer.setMontantTransfer(BigDecimal.valueOf(9.00));
//		when(compteRepository.findByNrCompte(any())).thenReturn(listCompte.get(0), listCompte.get(1));
//        doReturn(transfer).when(transferRepository).save(any());
//
//        TransactionException thrown = Assertions.assertThrows(TransactionException.class,() -> {transferService.createTransfer(Transfer.map(transfer));} ,"The amount should not be lesser than 10");
//
//        verify(compteRepository, times(2)).findByNrCompte(any());
//        Assertions.assertEquals("Montant minimal de transfer non atteint", thrown.getMessage());
//    }
	
//	@Test
//    @DisplayName("Test not save Transfer with amount greater than 10000")
//    public void should_not_save_transfer_if_not_null_and_amount_greater_than_10000() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
//		this.transfer.setMontantTransfer(BigDecimal.valueOf(100000.00));
//		when(compteRepository.findByNrCompte(any())).thenReturn(listCompte.get(0), listCompte.get(1));
//        doReturn(transfer).when(transferRepository).save(any());
//
//        TransactionException thrown = Assertions.assertThrows(TransactionException.class,() -> {transferService.createTransfer(Transfer.map(transfer));} ,"The amount should not be lesser than 10");
//
//        verify(compteRepository, times(2)).findByNrCompte(any());
//        Assertions.assertEquals("Montant maximal de transfer dépassé", thrown.getMessage());
//    }
	
//	@Test
//    @DisplayName("Test not save Transfer with amount = 0")
//    public void should_not_save_transfer_if_not_null_and_amount_equals_0() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
//		this.transfer.setMontantTransfer(BigDecimal.valueOf(0.00));
//		when(compteRepository.findByNrCompte(any())).thenReturn(listCompte.get(0), listCompte.get(1));
//        doReturn(transfer).when(transferRepository).save(any());
//
//        TransactionException thrown = Assertions.assertThrows(TransactionException.class,() -> {transferService.createTransfer(Transfer.map(transfer));} ,"The amount should not be lesser than 10");
//
//        verify(compteRepository, times(2)).findByNrCompte(any());
//        Assertions.assertEquals("Montant vide", thrown.getMessage());
//    }
	
	@Test
	@DisplayName("Test all transfers")
    public void should_return_all_transfers() {
        doReturn(Collections.singletonList(transfer)).when(transferRepository).findAll();
        List<TransferDto> depositDtos = transferService.loadAllTransfer().get();
        Assertions.assertEquals(1, depositDtos.size());
    }

}
