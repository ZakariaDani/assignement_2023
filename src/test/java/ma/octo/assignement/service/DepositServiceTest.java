package ma.octo.assignement.service;

import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.DepositRepository;

@ExtendWith(MockitoExtension.class)
public class DepositServiceTest {


	@Mock
	private DepositRepository depositRepository;
	@Mock
	private CompteRepository compteRepository;
	@InjectMocks
	private DepositService depositService;


	
	private DepositDto depositDto;
	private Deposit deposit;
	private Compte compte;
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		this.compte = Compte.builder()
				.rib("RIB1")
				.user(null)
				.solde(BigDecimal.valueOf(10000.00))
				.nrCompte("010000A000001000")
				.build();
		this.deposit = Deposit.builder()
				.compteBeneficiaire(compte)
				.dateExecution(new Date())
				.motifTransfer("2022")
				.nom_prenom_emetteur("Zakaria Dani")
				.build();
	    this.depositDto = DepositDto.builder()
				.date(new Date())
				.motif("2022")
				.nomPrenomEmetteur("Zakaria Dani")
				.rib("RIB1")
				.build();
	}
	
	@Test
    @DisplayName("Test save Deposit with amount greater than 10 and less than 10000")
    public void should_save_deposit_if_not_null_and_amount_greater_than_10_and_lesser_than_10000() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		// Setup our mock repository
		this.depositDto.setMontant(BigDecimal.valueOf(100.00));
		this.deposit.setMontantTransfer(BigDecimal.valueOf(100.00));
		when(compteRepository.findByRib(compte.getRib())).thenReturn(Optional.ofNullable(compte));
		doReturn(deposit).when(depositRepository).save(any());
		DepositDto returnedDeposit = depositService.createDeposit(depositDto);
        Assertions.assertNotNull(returnedDeposit, "The saved deposit should not be null");
        Assertions.assertEquals(BigDecimal.valueOf(100.00), returnedDeposit.getMontant(), "The amount");
    }

//	@Test
//	@DisplayName("Test not save Deposit with amount lesser than 10")
//    public void should_not_save_deposit_if_not_null_and_amount_lesser_than_10() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
//		this.deposit.setMontantTransfer(BigDecimal.valueOf(9.00));
//		this.depositDto.setMontant(BigDecimal.valueOf(9.00));
//		when(compteRepository.findByRib(compte.getRib())).thenReturn(Optional.ofNullable(compte));
//		doReturn(deposit).when(depositRepository).save(any());
//		depositService.createDeposit(depositDto);
//		verify(depositRepository, times(0)).save(deposit);
//    }
	
//	@Test
//	@DisplayName("Test not save Deposit with amount greater than 10000")
//    public void should_not_save_deposit_if_not_null_and_amount_greater_than_10000() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
//		this.deposit.setMontantTransfer(BigDecimal.valueOf(90000.00));
//        doReturn(deposit).when(depositRepository).save(any());
//        TransactionException thrown = Assertions.assertThrows(TransactionException.class,() -> {depositService.createDeposit(Deposit.map(deposit));} ,"The amount should not be greater than 10000");
//        Assertions.assertEquals("Montant maximal de deposit dépassé", thrown.getMessage());
//    }
	
//	@Test
//	@DisplayName("Test not save Deposit with amount = 0")
//    public void should_not_save_deposit_if_not_null_and_amount_equals_0() throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
//		this.deposit.setMontantTransfer(BigDecimal.valueOf(0.00));
//        doReturn(deposit).when(depositRepository).save(any());
//        TransactionException thrown = Assertions.assertThrows(TransactionException.class,() -> {depositService.createDeposit(Deposit.map(deposit));} ,"The amount should not be zero");
//        Assertions.assertEquals("Montant vide", thrown.getMessage());
//    }
	
	@Test
	@DisplayName("Test all deposits")
    public void should_return_all_deposits() {
        doReturn(Collections.singletonList(deposit)).when(depositRepository).findAll();
        List<DepositDto> depositDtos = depositService.loadAllDeposit().get();
        Assertions.assertEquals(1, depositDtos.size());
    }
}
