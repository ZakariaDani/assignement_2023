package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class CompteServiceTest {

    @InjectMocks
    private CompteService compteService;

    @Mock
    private CompteRepository compteRepository;

    private Compte compte;
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.compte = Compte.builder()
                .rib("RIB1")
                .nrCompte("010000A000001000")
                .solde(BigDecimal.valueOf(100000.00))
                .user(User.builder()
                        .username("user1")
                        .firstname("Zakaria")
                        .lastname("Dani")
                        .password("1234")
                        .gender("Male")
                        .build())
                .build();
    }
    
    @Test
    @DisplayName("Test all accounts")
    public void should_return_all_accounts() {
        doReturn(Collections.singletonList(compte)).when(compteRepository).findAll();
        List<CompteDto> compteDtos = compteService.loadAllCompte().get();
        Assertions.assertEquals(1, compteDtos.size());
    }
}
