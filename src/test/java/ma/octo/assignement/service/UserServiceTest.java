package ma.octo.assignement.service;

import ma.octo.assignement.domain.User;
import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;

    private User user;
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.user = User.builder()
                .username("user10")
                .firstname("Zakaria")
                .lastname("Dani")
                .password("1234")
                .gender("Male")
                .build();
    }
    @Test
    @DisplayName("Test all users")
    public void should_return_all_users() {
        doReturn(Collections.singletonList(user)).when(userRepository).findAll();
        List<UserDto> userDtos = userService.loadAllUsers().get();
        Assertions.assertEquals(1, userDtos.size());
    }
}
