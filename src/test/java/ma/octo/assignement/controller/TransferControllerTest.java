package ma.octo.assignement.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.service.TransferService;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class TransferControllerTest {

	@MockBean
	private TransferService transferService;
	@Autowired
    private MockMvc mockMvc;
	private TransferDto transferDto;

	@BeforeEach
	public void setUp() {
		transferDto = TransferDto.builder()
				.montant(BigDecimal.valueOf(100.00))
				.motif("2022")
				.date(new Date())
				.nrCompteBeneficiaire("010000A000001000")
				.nrCompteEmetteur("010000B000001000")
				.build();
	}
	@Test
    @DisplayName("POST /transfers")
    void should_create_transfer() throws Exception {
        doReturn(transferDto).when(transferService).createTransfer(any());
        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(transferDto)).accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());
		verify(transferService, times(1)).createTransfer(any());
    }

	@Test
	@DisplayName("GET /transfers")
	public void should_get_all_transfers() throws Exception {
		when(transferService.loadAllTransfer()).thenReturn(Optional.of(Collections.singletonList(transferDto)));
		mockMvc.perform(get("/transfers"))
				.andExpect(status().isOk());
		verify(transferService, times(1)).loadAllTransfer();
	}

	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
