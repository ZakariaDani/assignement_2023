package ma.octo.assignement.controller;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.service.CompteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class CompteControllerTest {
    @MockBean
    private CompteService compteService;

    @Autowired
    private MockMvc mockMvc;

    private CompteDto compteDto;
    @BeforeEach
    public void setUp() {
        this.compteDto = CompteDto.builder()
                .nrCompte("010000A000001000")
                .rib("RIB1")
                .solde(BigDecimal.valueOf(100000.00))
                .build();
    }
    @Test
    @DisplayName("GET /accounts")
    public void should_get_all_accounts() throws Exception {
        when(compteService.loadAllCompte()).thenReturn(Optional.of(Collections.singletonList(compteDto)));
        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk());
        verify(compteService, times(1)).loadAllCompte();
    }
}
