package ma.octo.assignement.controller;

import static org.mockito.ArgumentMatchers.any;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.service.DepositService;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class DepositControllerTest {
	
	@MockBean
	private DepositService depositService;
	
	@Autowired
    private MockMvc mockMvc;

	private DepositDto depositDto;

	@BeforeEach
	public void setUp() {
		this.depositDto = DepositDto.builder()
				.montant(BigDecimal.valueOf(100.00))
				.motif("2022")
				.nomPrenomEmetteur("Zakaria Dani")
				.rib("RIB1")
				.date(new Date())
				.build();
	}
	@Test
    @DisplayName("POST /deposits")
    void should_create_deposit() throws Exception {
		doReturn(depositDto).when(depositService).createDeposit(any());
		mockMvc.perform(post("/deposits")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(depositDto)).accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());
		verify(depositService, times(1)).createDeposit(any());
    }

	@Test
	@DisplayName("GET /deposits")
	public void should_get_all_deposits() throws Exception {
		when(depositService.loadAllDeposit()).thenReturn(Optional.of(Collections.singletonList(depositDto)));
		mockMvc.perform(get("/deposits"))
				.andExpect(status().isOk());
		verify(depositService, times(1)).loadAllDeposit();
	}
	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
