package ma.octo.assignement.controller;

import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class UserControllerTest {
    @MockBean
    private UserService userService;
    @Autowired
    private MockMvc mockMvc;
    
    private UserDto userDto;

    @BeforeEach
    public void setUp() {
        this.userDto = UserDto.builder()
                .birthdate(new Date())
                .firstname("Zakaria")
                .lastname("Dani")
                .gender("Male")
                .build();
    }
    @Test
    @DisplayName("GET /users")
    public void should_get_all_users() throws Exception {
        when(userService.loadAllUsers()).thenReturn(Optional.of(Collections.singletonList(userDto)));
        mockMvc.perform(get("/users"))
                .andExpect(status().isOk());
        verify(userService, times(1)).loadAllUsers();
    }
}
